#!/bin/bash

GITTIME=$(git log -n 1 --pretty=%cd --date=format:%y-%m-%d_%H-%M-%S)
GITDAY=$(git log -n 1 --pretty=%cd --date=format:%Y-%m-%d)
GITSHA=$(git log -n 1 --pretty=%H)
GITSHASHORT=$(git log -n 1 --pretty=%h)

# make sure the script is run from the root project directory!
source web/config.sh

mkdir -p build_diff/$GITSHA

for (( i = 0; i < $NUMDIFF; i++ )); do
    TARSHA=$(git log -n $((i+2)) --pretty=%H | tail -n 1)
    TARSHASHORT=$(git log -n $((i+2)) --pretty=%h | tail -n 1)
    # TARMSG=$(git show --pretty="%B" -s $GITSHA)
    # TARLOG=$(git show -s $GITSHA)
    # TARDAY=$(git show --pretty=%cd --date=format:%Y-%m-%d -s $GITSHA)
    # TARTIME=$(git show --pretty=%cd --date=format:%y-%m-%d_%H-%M-%S -s $GITSHA)

    THISDIF="build_diff/$GITSHA/$DIFFPTRN"_"$GITSHASHORT"_"$TARSHASHORT".pdf

    git latexdiff $TARSHA --bbl --ignore-latex-errors -o $THISDIF
done
